package com.example.fragmentdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    EditText edtSoThuNhat, edtSoThuHai;
    Button btnResult;
    TextView txtView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AnhXa();

        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s1 = edtSoThuNhat.getText().toString();
                String s2 = edtSoThuHai.getText().toString();

                Random random = new Random();

                int min = Integer.parseInt(s1);
                int max = Integer.parseInt(s2);

                int randomNum = random.nextInt(Math.abs(max - min )) + min;

                txtView.setText(String.valueOf(randomNum));

            }
        });

    }
    private void AnhXa(){
        edtSoThuNhat = (EditText) findViewById(R.id.edtSoThuNhat);
        edtSoThuHai = (EditText) findViewById(R.id.edtSoThuHai);
        btnResult = (Button) findViewById(R.id.button);
        txtView = (TextView) findViewById(R.id.textView);
    }
}